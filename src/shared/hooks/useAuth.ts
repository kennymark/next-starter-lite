import { useStoreActions, useStoreState } from '@store/hooks'
import { iAuth } from '@store/auth'

function useAuth(): iAuth {
  const auth = useStoreState((state) => state.auth)
  const setToken = useStoreActions((state) => state.auth.setToken)
  const setUser = useStoreActions((state) => state.auth.setUser)
  const setExpiresAt = useStoreActions((state) => state.auth.setExpiresAt)

  return { ...auth, setUser, setToken, setExpiresAt }
}

export default useAuth
