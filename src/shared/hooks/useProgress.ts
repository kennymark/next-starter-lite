
import NProgress from 'nprogress';
import { useState } from 'react';

function useProgress() {
  const [isLoading, setLoading] = useState(false)

  const startProgress = () => {
    setLoading(true)
    NProgress.start()
  }
  const stopProgress = () => {
    setLoading(false)
    NProgress.done()
  }

  return { startProgress, stopProgress, isLoading }

}

export default useProgress