import { extendTheme } from '@chakra-ui/react'

const customeTheme = extendTheme({
  colors: {
    accent: '#ffedc5',
    selected: '#38a169',
    darkMode: '#111216',
    focusBorderColor: 'black',
    alt: 'blue.600',
    danger: 'red.600',
    lightBlack: 'gray.600',
  },
  fonts: {
    body: 'Inter, sans-serif',
    // heading: "IBM Plex Sans, sans-serif",
    // monospace: "Source Code Pro, monospace",
  },
  fontSizes: {},

  components: {
    Link: {
      baseStyle: {
        textDecoration: 'none',
        _hover: {
          color: 'blue.400',
        },
      },
    },
    Button: {
      baseStyle: {
        borderRadius: 'lg',
        fontWeight: 'normal',
        minWidth: 40,
      },
      sizes: {
        main: {
          minWidth: 60,
        },
      },
      defaultProps: {
        variant: 'solid',
      },
      variants: {
        plain: {
          bg: 'white',
          border: '1px',
          borderColor: 'gray.200',
          fontWeight: 'normal',
          _focus: { boxShadow: 'none' },
          _active: { boxShadow: 'none' },
          _hover: { bg: 'gray.50', shadow: 'sm' },
        },
        danger: {
          color: 'white',
          bg: 'red.500',
          height: 50,
          _focus: { boxShadow: 'none' },
          _hover: { bg: 'red.400' },
          _active: { boxShadow: 'none' },
        },
        main: {
          color: 'white',
          _hover: { bg: 'gray.800', color: 'gray.400', borderColor: 0 },
          _active: { bg: 'gray.700', shadow: 'md', outline: 0 },
          _focus: { boxShadow: 'none' },
          _disabled: { _hover: { color: 'black', bg: 'black' } },
          bg: 'gray.900',
          width: '100%',
          height: 50,
          mt: 4,
        },
        secondary: {
          _hover: { bg: 'gray.50', color: 'gray.600' },
          border: '1px',
          borderColor: 'gray.200',
          _active: { bg: 'gray.100', shadow: 'sm' },
          _focus: { boxShadow: 'none' },
          bg: 'white',
          width: '100%',
          height: 50,
          mt: 4,
        },
      },
    },
    FormLabel: {
      baseStyle: {
        color: 'gray.600',
        fontSize: 'sm',
      },
    },
    Input: {
      baseStyle: {
        borderRadius: 'lg',
        color: 'black',
        borderColor: 'gray.300',
        minWidth: 350,
        _focus: { border: '1px', boxShadow: 'none' },
      },

      defaultProps: {
        size: 'lg',
        variant: 'outline',
      },
      variants: {
        outline: {
          field: {
            borderColor: 'gray.200',
            border: '1px',
            _disabled: {
              bg: 'gray.50',
              cursor: 'not-allowed',
            },
            _focus: { borderColor: 'green.900' },
            _hover: { border: '1px', borderColor: 'gray.400' },
          },
        },
      },
    },
  },
  config: {
    initialColorMode: 'light',
    useSystemColorMode: false,
  },
  styles: {
    'html, body': {
      textDecoration: 'none',
      scrollBehavior: 'smooth',
      '*:focus': {
        outline: '0 !important',
        boxShadow: 'none !important',
      },
    },
    global: (props) => ({
      a: {
        textDecoration: 'none',
      },
      pre: { fontFamily: 'monospace' },
      body: {
        bg: props.colorMode === 'light' ? 'white' : 'darwkMode',
      },
    }),
  },
})

export default customeTheme
