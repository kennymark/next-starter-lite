// @ts-nocheck
import Yupper from './form'

export const forgotPasswordSchema = Yupper.object().shape({
  email: Yupper.string().isEmail(),
})

export const signInSchema = Yupper.object().shape({
  email: Yupper.string().isEmail(),
  password: Yupper.string().customValidation(),
})

export const signUpSchema = Yupper.object().shape({
  name: Yupper.string().customValidation(),
  email: Yupper.string().isEmail(),
  password: Yupper.string().customValidation(),
  password_confirmation: Yupper.string().oneOf([Yupper.ref('password')], 'Passwords must match'),
})

export const resetPasswordSchema = Yupper.object().shape({
  password: Yupper.string().customValidation(),
  password_confirmation: Yupper.string().oneOf([Yupper.ref('password')], 'Passwords must match'),
})

export const accountSchema = Yupper.object().shape({
  name: Yupper.string().customValidation(),
  email: Yupper.string().isEmail(),
})

export const updatePasswordSchema = Yupper.object({
  password: Yupper.string()
    .customValidation()
    .notOneOf([Yupper.ref('new_password')], 'Your old and new password are the same'),
  new_password: Yupper.string().customValidation(),
  password_confirmation: Yupper.string()
    .customValidation()
    .oneOf([Yupper.ref('new_password')], 'Passwords must match'),
})

export const resendEmailSchema = Yupper.object().shape({
  email: Yupper.string().isEmail(),
  confirm_email: Yupper.string().oneOf([Yupper.ref('email')], 'Emails must match'),
})
