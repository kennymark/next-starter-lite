import * as yup from 'yup'

function customValidation(min = 4, max = 50): yup.StringSchema {
  return this.min(4).max(50).required()
}

function isEmail(): yup.StringSchema {
  return this.email().customValidation('Email')
}

export const isButtonEnabled = (formik): boolean => !formik.isValid || !formik.dirty

yup.addMethod<yup.StringSchema>(yup.string, 'customValidation', customValidation)
yup.addMethod<yup.StringSchema>(yup.string, 'isEmail', isEmail)

const Yupper = yup

export default Yupper
