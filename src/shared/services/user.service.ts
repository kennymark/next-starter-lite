import api from './http.service'
import { User } from '../models/user.model'

class UserService {
  getCurrentUser() {
    return api.get('user/me')
  }

  getTeam() {
    return api.get('user/team')
  }

  updateUser(user: User) {
    return api.put('user/me', user)
  }
}

export default new UserService()
