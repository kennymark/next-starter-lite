import { Container, Stack } from '@chakra-ui/react'
import SEO from '@components/seo'
import Footer from './footer'
import Header from './nav'

interface Props {
  sm?: boolean
  children?: React.ReactNode
  title?: string
}

function Layout({ children, sm, title }: Props) {
  return (
    <Stack>
      <SEO title={title} />
      <Header />
      <Container p={4} minW={sm ? '2xl' : '4xl'} minH='90vh'>
        {children}
      </Container>
      <Footer />
    </Stack>
  )
}

export default Layout
