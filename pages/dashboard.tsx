import { Heading } from '@chakra-ui/react'
import Layout from '@components/layout'
import React from 'react'

function Dashboard() {
  return (
    <Layout title='Dashboard'>
      <Heading>Dashboard</Heading>
    </Layout>
  )
}

export default Dashboard
