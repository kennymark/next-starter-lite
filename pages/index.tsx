import { Flex, Img } from '@chakra-ui/react'
import Layout from '@components/layout'

function IndexPage() {
  return (
    <Layout title='Home'>
      <Flex>
        <Img src='team-of-critters.svg' maxW='full' alt='Four one-eyed aliens playing' />
      </Flex>
    </Layout>
  )
}

export default IndexPage
