import { ChakraProvider } from '@chakra-ui/react'
import store from 'src/shared/store'
import theme from 'src/shared/utils/theme'
import { StoreProvider } from 'easy-peasy'
import { Router } from 'next/router'
import React, { useEffect } from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'
import useProgress from 'src/shared/hooks/useProgress'
import 'src/css/index.css'
import 'src/css/nprogress.css'

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: { retry: 2 },
  },
})

const MyApp = ({ Component, pageProps }) => {
  const { startProgress, stopProgress } = useProgress()

  useEffect(() => {
    Router.events.on('routeChangeStart', (_) => startProgress())
    Router.events.on('routeChangeComplete', (_) => stopProgress())
    Router.events.on('routeChangeError', (_) => stopProgress())

    return () => {
      Router.events.off('routeChangeStart', (_) => stopProgress())
      Router.events.off('routeChangeComplete', (_) => stopProgress())
      Router.events.off('routeChangeError', (_) => stopProgress())
    }
  })

  return (
    <ChakraProvider theme={theme}>
      <StoreProvider store={store}>
        <QueryClientProvider client={queryClient}>
          <Component {...pageProps} />
          <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
      </StoreProvider>
    </ChakraProvider>
  )
}

export default MyApp
